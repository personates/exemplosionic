// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services','ngCordova'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  // setup an abstract state for the tabs directive
    .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabs.html'
  })

  // Each tab has its own nav history stack:

  .state('tab.aula01', {
    url: '/dash',
    views: {
      'tab-dash1': {
        templateUrl: 'templates/aula01.html',
        controller: 'DashCtrl'
      }
    }
  })

  .state('tab.aula02', {
      url: '/aula02',
      views: {
        'tab-dash2': {
          templateUrl: 'templates/aula02.html',
          controller: 'Aula02Ctrl'
        }
      }
    })
    .state('tab.aula03', {
      url: '/aula03',
      views: {
        'tab-dash3': {
          templateUrl: 'templates/aula03.html',
          controller: 'Aula03Ctrl'
        }
      }
    })

  .state('tab.account', {
    url: '/account',
    views: {
      'tab-chats': {
        templateUrl: 'templates/tab-account.html',
        controller: 'AccountCtrl'
      }
    }
  })
    .state('tab.restaurantes', {
      url: '/restaurantes',
      views: {
        'tab-dash4': {
          templateUrl: 'templates/restaurantes.html',
          controller: 'restauranteController'
        }
      }
    })
  .state('tab.favoritos', {
    url: '/favoritos',
    views: {
      'tab-dash5': {
        templateUrl: 'templates/restaurantes.html',
        controller: 'favoritosController'
      }
    }
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/tab/restaurantes');

});
