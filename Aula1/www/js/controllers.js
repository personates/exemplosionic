angular.module('starter.controllers', ['ngCordova'])

.controller('DashCtrl', function($scope, $rootScope, $ionicPlatform, $cordovaBatteryStatus,$cordovaDevice) {
  console.log('aula01');
/*Primeiramente fora temer.
Depois disso, vamos detectar os eventos de ciclo de vida e avisar ao usuario.
Voces perceberão que os alertas mal aparecem.
Podemos coloca-los dentro do aplicativo para ver mais facil
 */
  document.addEventListener("pause", function () {
    //alert('Fica! Vai ter Bolo!');
  });
  document.addEventListener("resume", function () {
    //alert('Estava com saudades!');
  });
  document.addEventListener("unload", function () {
    //alert('É hora de dar tchau!');
  });

  /*
  Antes de fazer qualquer uso de plugin, temos que ver se o aparelho esta pronto, deviceready
   */
  document.addEventListener("deviceready", function () {

    /*peguemo os dados do celular com cordovaDevice*/
    $scope.device = $cordovaDevice.getDevice();
    $scope.cordova = $cordovaDevice.getCordova();
    $scope.model = $cordovaDevice.getModel();
    $scope.platform = $cordovaDevice.getPlatform();
    $scope.uuid = $cordovaDevice.getUUID();
    $scope.version = $cordovaDevice.getVersion();

    /*vamos monitorar o status da bateria*/
    $rootScope.$on("$cordovaBatteryStatus:status", function(event, args){

      $scope.batteryLevel = args.level;
      $scope.isPluggedIn = args.isPlugged;

      /*aqui poderiamos ver se esta carregando */
      if(args.isPlugged)
      {
        //alert("Charging -> " + args.level + " %");
      }
      else
      {
        //alert("Battery -> " + args.level + " %");
      }

      /* uma firula para mostrar bonito o status da bateria*/
      $scope.percentageStyle = {
        width : $scope.batteryLevel + '%'
      };
    });

  });
})

.controller('Aula02Ctrl', function($scope , $rootScope, $cordovaDeviceMotion , $cordovaDeviceOrientation , $cordovaNetwork, $cordovaVibration) {
  console.log('aula02');
  /*ja aprenderam, ne?
  Se quer usar plugin, tem que ver se o aparelho esta pronto, deviceready */

  document.addEventListener("deviceready", function () {

    // frequencia de verificacao em milisegundos
    var options = { frequency: 3000 };

    /*pegar a posicao atual, x, y e z*/
    $cordovaDeviceMotion.getCurrentAcceleration().then(function(result) {
      $scope.X = result.x;
      $scope.Y = result.y;
      $scope.Z = result.z;
      $scope.timeStamp = result.timestamp;
    }, function(err) {

    });


    /*pegar a direcao do celular*/
    $cordovaDeviceOrientation.getCurrentHeading().then(function(result) {
      $scope.magneticHeading = result.magneticHeading;
      $scope.trueHeading = result.trueHeading;
      $scope.accuracy = result.headingAccuracy;
      $scope.timeStamp = result.timestamp;
    }, function(err) {
      alert('erro 1.1');
    });

    /* monitorar o x y e z*/
    var watch = $cordovaDeviceMotion.watchAcceleration(options);
    watch.then(null,
      function(error) {
        alert(' erro 1.2' );
      },function(result) {
          $scope.X = result.x;
          $scope.Y = result.y;
          $scope.Z = result.z;
          $scope.timeStamp = result.timestamp;
        });

    /*agora é a frequencia para a bussola*/
    var options = {
      frequency: 3000,
      filter: true     // if frequency is set, filter is ignored
    };

    /*monitorar a bussola*/
    var watch = $cordovaDeviceOrientation.watchHeading(options).then(
      null,
      function(error) {
        alert(' erro 1.3' );
      },function(result){
          $scope.magneticHeading = result.magneticHeading;
          $scope.trueHeading = result.trueHeading;
          $scope.accuracy = result.headingAccuracy;
          $scope.timeStamp = result.timestamp;
        });


    /*internet
    podemos pegar o tipo: 3g, wifi, etc, pelo type
    nosso objetivo é online e offline
     */
    $scope.type = $cordovaNetwork.getNetwork();
    $scope.isOnline = $cordovaNetwork.isOnline();
    $scope.isOffline = $cordovaNetwork.isOffline();

    /*detecta se esta sendo acessado via web*/
    if(ionic.Platform.isWebView()){

      /*monitores online e offline*/
      $rootScope.$on("$cordovaNetwork:online", function(event, networkState){
        $scope.isOnline=true;
        $cordovaVibration.vibrate(100);
      });

      $rootScope.$on("$cordovaNetwork:offline", function(event, networkState){
        $scope.isOnline=false;
        $cordovaVibration.vibrate(100);
      });

    }
    else {
      /*monitores online e offline*/
      window.addEventListener("online", function(e) {
        $scope.isOnline=true;
        $cordovaVibration.vibrate(100);
      }, false);

      window.addEventListener("offline", function(e) {
        $scope.isOnline=false;
        $cordovaVibration.vibrate(100);
      }, false);
    }
  }, false);




})

.controller('Aula03Ctrl', function($scope, $rootScope, $cordovaCamera, $cordovaCapture) {
  console.log('aula03');


  document.addEventListener("deviceready", function () {

    var options = {
      quality: 50,
      destinationType: Camera.DestinationType.DATA_URL,
      sourceType: Camera.PictureSourceType.CAMERA,
      allowEdit: true,
      encodingType: Camera.EncodingType.JPEG,
      targetWidth: 100,
      targetHeight: 100,
      popoverOptions: CameraPopoverOptions,
      saveToPhotoAlbum: true,
      correctOrientation:true
    };
    $scope.tirafoto = function(){
      alert('aqui ');
      $cordovaCamera.getPicture(options).then(function (imageData) {
        var image = document.getElementById('myImage');
        image.src = "data:image/jpeg;base64," + imageData;
      }, function (err) {
        alert('error camera');
      });
    };
  });
})

.controller('AccountCtrl', function($scope) {
  $scope.settings = {
    enableFriends: true
  };
})
  .controller('restauranteController',function($scope, aulaTP3) {
    $scope.titulo = 'Todos';
    $scope.restaurantes = [];
    $scope.favoritos = [];
    aulaTP3.retornaRestaurantes().then(function (x) {
      $scope.restaurantes = x.data.restaurants;
      console.log(x);
      $scope.favoritar = function (x) {
        $scope.favoritos.push(x);
        localStorage.setItem('restaurantesInfnet', JSON.stringify($scope.favoritos));
        //localStorage.setItem('restaurantesInfnet', JSON.stringify(x));
      };

    })
  })
      .controller('favoritosController',function($scope, aulaTP3){
        $scope.titulo= 'Favoritos';
        $scope.restaurantes=JSON.parse(localStorage.getItem('restaurantesInfnet'));
        console.log($scope.restaurantes);




  });
